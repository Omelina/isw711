const express = require('express');
const cors = require("cors");
const app = express();
const path = require('path');
const router = express.Router();

// check for cors
app.use(cors({
    domains: '*',
    methods: "*"
}));

app.get('/',function(req,res) {
    res.sendFile('index.html', { root: 'D:\\Docs\\ISW711\\IntroNode\\client'});
});

//add the router
app.use('/', router);
app.listen(process.env.port || 3001);

// listen to GET requests on /hello
app.get('/hello', function (req, res) {
    res.send('Hello World');
});


app.get('/tipocambio', function (req, res) {
    res.send(`{
    "TipoCompraDolares" : "608",
    "TipoVentaDolares" : "621",
    "TipoCompraEuros" : "731.85",
    "TipoVentaEuros" : "761.9"
}`);
});


app.listen(3000, () => console.log(`Example app listening on port 3000!`))