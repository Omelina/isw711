getTasks();
getLS()

function getTasks() {
    fetch(`http://localhost:3000/tasks`, {
        headers: {
            Authorization: 'Basic YWRtaW46MTIzNA=='
        }
        
    })
        .then((response) => response.json())
        .then((data) => {
            showData(data);
        })
}

function showData(tasks) {
    const container = document.getElementById('tasks');
    tasks.forEach(item => {
        container.innerHTML += `
        <div class="card m-3">
            <div class="card-body">
                <h1 class="card-title">${item.title}</h1>
                <p class="card-subtitle mb-2 text-muted">${item.description}</p>
                <a href="http://127.0.0.1:5500/Workshop9/client/addtask.html"
                 onclick="saveLS('${item._id}')" class="btn btn-primary mt-2">Edit</a>
                <a href="#" onclick="eraseOne('${item._id}');" class="btn btn-outline-danger mt-2">Delete</a>
            </div>
        </div>`
    });
}

function saveLS(itemId) {
    localStorage.setItem("id", itemId);
    localStorage.setItem("status", true);
}

function getLS() {
    const id = localStorage.getItem("id");
    try {
        getOne(id);
    } catch (err) {
        console.log("You can continue");
    }
}

function getOne(id) {
    const url = `http://localhost:3000/tasks?id=${id}`;
    // const url = 'http://localhost:3000/tasks?id=60f1eed245c3ea0678f56b69';
    console.log(url);
    fetch(url, {
        headers:{
            Authorization: 'Basic YWRtaW46MTIzNA=='
        }
    })
        .then((response) => response.json())
        .then((data) => {
            console.log(data);
            document.getElementById("title").value = data.title;
            document.getElementById("description").value = data.description;
        })
}

function eraseOne(id) {
    const url = `http://localhost:3000/tasks?id=${id}`;
    fetch(url, {
            method: 'delete',
            body: JSON.stringify({}),
            headers: {
                Authorization: 'Basic YWRtaW46MTIzNA==',
                "Content-type": "application/json; charset=UTF-8"
            }
        })
        .then((response) => response.json())
        .then((data) => {
            console.log(data);
            window.location.reload();
        })
};

function edit(id) {
    const title = document.getElementById('title').value;
    const description = document.getElementById('description').value;
    fetch(`http://localhost:3000/tasks?id=${id}`, {
            method: 'PATCH',
            headers: {
                Authorization: 'Basic YWRtaW46MTIzNA==',
                "Content-type": "application/json; charset=UTF-8"
            },
            body: JSON.stringify({
                title: title,
                description: description
            }),
        })
        .then((response) => response.json())
        .then((data) => {
            console.log(data);
            window.history.back();
        })
}

function create() {
    const title = document.getElementById('title').value;
    const description = document.getElementById('description').value;
    const s = localStorage.getItem("status");
    if (s ===false) {
        fetch(`http://localhost:3000/tasks`, {
                method: 'post',
                body: JSON.stringify({
                    title: title,
                    description: description
                }),
                headers: {
                    Authorization: 'Basic YWRtaW46MTIzNA==',
                    "Content-type": "application/json; charset=UTF-8"
                }
            })
            .then((response) => response.json())
            .then((data) => {
                console.log(data);
                window.history.back();
            })
    } else {
        const id = localStorage.getItem("id");
        edit(id)
    }



}