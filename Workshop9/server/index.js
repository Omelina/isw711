const express = require('express');
const cors = require("cors");
const app = express();
const mongoose = require("mongoose");
const db = mongoose.connect("mongodb://127.0.0.1:27017/workshop9");
const bodyParser = require("body-parser");
const Task = require("./models/tasksModel");
const Session = require("./models/sessionsModel");
// const { saveSession} = require("./models/sessionsModel");
const {
    base64decode
  } = require('nodejs-base64');
const path = require('path');
const router = express.Router();


// check for cors
app.use(cors({
    domains: "*",
    methods: "*"
}));
// app.use("/client", express.static('./client/'));

app.get('/',function(req,res) {
    res.sendFile('index.html', { root: 'D:\\Documents\\ISW711\\Workshop9\\client'});
});

//add the router
app.use('/', router);
app.listen(process.env.port || 3001);



const {
    taskPatch,
    taskPost,
    taskGet,
    taskDelete
} = require("./controllers/taskController.js");
const { saveSession } = require('./controllers/sessionController');

// parser for the request body (required for the POST and PUT methods)
app.use(bodyParser.json());

app.use(function (req, res, next) {
    if (req.headers["authorization"]) {
      // Basic VVROOlBhc3N3b3JkMQ==
  
      const authBase64 = req.headers['authorization'].split(' ');
      console.log('authBase64:', authBase64);
      const userPass = base64decode(authBase64[1]);
      console.log('userPass:', userPass);
      const user = userPass.split(':')[0];
      const password = userPass.split(':')[1];
  
      if (user === 'admin' && password == '1234') {
        // saveSession('admin');
        next();
        return;
      }
    }
    res.status(401);
    res.send({
      error: "Unauthorized"
    });
  });

app.get("/tasks", taskGet);
app.post("/tasks", taskPost);
app.patch("/tasks", taskPatch);
app.post("/tasks", taskPatch);
app.delete("/tasks", taskDelete);


app.listen(3000, () => console.log(`Example app listening on port 3000!`))