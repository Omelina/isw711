const express = require('express');
const cors = require("cors");
const app = express();
const mongoose = require("mongoose");
const db = mongoose.connect("mongodb://127.0.0.1:27017/workshop1");
const bodyParser = require("body-parser");
const Task = require("./models/tasksModel");


// check for cors
app.use(cors({
    domains: '*',
    methods: "*"
}));

const {
    taskPatch,
    taskPost,
    taskGet,
    taskDelete
} = require("./controllers/taskController.js");

// parser for the request body (required for the POST and PUT methods)
app.use(bodyParser.json());

// listen to the task request
app.get("/tasks", taskGet);
app.post("/tasks", taskPost);
app.patch("/tasks", taskPatch);
app.post("/tasks", taskPatch);
app.delete("/tasks", taskDelete);


app.listen(3000, () => console.log(`Example app listening on port 3000!`))