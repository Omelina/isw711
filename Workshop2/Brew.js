const numBrew = 10;
getBreweries();
function getBreweries() {
   fetch(`https://api.openbrewerydb.org/breweries?per_page=${numBrew}`)
        .then((res) => res.json())
        .then((data) => {
            showData(data);
        })
}

function showData(breweries) {
    const container = document.querySelector('.row');
    breweries.forEach(item => {
        container.innerHTML += `
        <div class="card m-3">
            <div class="card-body">
                <p id="result" class="card-header">${item.name}</p>
                <p class="card-text ">${item.city}</p>
                <a href="#" onclick="OneBrewerie(${item.id})" class="card-link">More Info</a>
                <p></p>
            </div>
        </div>`
    });
}

function OneBrewerie(id) {
    const url = `https://api.openbrewerydb.org/breweries/${id}`
    fetch(url).then(response => response.json()).then(data => {
        const container = document.getElementById("brew");
            container.innerHTML = `
            <div class="card m-3">
                <div class="card-body">
                    <p id="result" class="card-header">${data.name}</p>
                    <p class="card-text ">${data.city}</p>
                    <p class="card-text ">${data.state}</p>
                    <p class="card-text ">${data.phone}</p>
                </div>
            </div>`
    });
}
    