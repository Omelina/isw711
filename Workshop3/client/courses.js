getCourses();

function getCourses() {
    fetch(`http://localhost:3000/courses`)
        .then((response) => response.json())
        .then((data) => {
            showData(data);
        })
}

function showData(courses) {
    const container = document.getElementById('course');
    courses.forEach(item => {
        container.innerHTML += `
        <div class="card m-3">
            <div class="card-body">
                <h1 class="card-title">${item.name}</h1>
                <p class="card-subtitle mb-2 text-muted">${item.code}</p>
                <p class="list-group-item">Carrera: ${item.career}</p>
                <p class="list-group-item">Creditos: ${item.credits}</p>
                <a href="http://127.0.0.1:5500/Workshop3/client/addcourse.html"
                 onclick="getOne('${item._id}')" class="btn btn-primary mt-2">Edit</a>
                <a href="#" onclick="eraseOne('${item._id}');" class="btn btn-outline-danger mt-2">Delete</a>
            </div>
        </div>`
    });
}

// function getOne(id){
//     console.log("hola");
//     if (id) {
//         // uriget = `http://localhost:3000/courses?id=${id}`;
//         uriget = `http://localhost:3000/courses`;
//     }
//     const ajaxRequest = new XMLHttpRequest();
//     ajaxRequest.addEventListener("load", (response)=>{
        
//         const course = JSON.parse(response.target.resposeText);
//         console.log(course);
//         // document.getElementById("code").value = course.code;
//         // document.getElementById("name").value = course.name;
//         // document.getElementById("career").value = course.career;
//         // document.getElementById("credits").value = course.credits;
//     });
//     ajaxRequest.addEventListener("error",error);
//     ajaxRequest.open("GET", uriget);
//     ajaxRequest.setRequestHeader("Content-Type", "application/json");
//     ajaxRequest.send();
// }

function getOne(id){
    // const url = `http://localhost:3000/courses?id=${id}`;
    const url = 'http://localhost:3000/coursesid=60c3ffb7a8c8dc0904cb6e7c';
    console.log(url);
    fetch(url)
        .then((response) => response.json())
        .then((data) => {
            console.log(data);
            document.getElementById("code").value = data.code;
            document.getElementById("name").value = data.name;
            document.getElementById("career").value = data.career;
            document.getElementById("credits").value = data.credits;
    //         var div = document.getElementById('erase');
    // div.parentNode.removeChild(div);
    //         const container = document.getElementById("edit");
    //         container.innerHTML = `
    //         <form>
    //             <div class="form-group">
    //                 <label >Code: </label>
    //                 <input value="${data.code}" type="text" name="code" id="code" class="form-control"  >
    //                 <label >Name: </label>
    //                 <input value="${data.name}" type="text" name="name" id="name" class="form-control"  >
    //                 <label >Career: </label>
    //                 <input value="${data.career}" type="text" name="career" id="career" class="form-control" >
    //                 <label >Credits: </label>
    //                 <input value="${data.credits}" type="text" name="credits" id="credits" class="form-control" >
    //             </div> 
    //         <button type="button" onclick="edit(${data.id})" class="btn btn-primary">Sign in</button>
    //         </form>`
        })
}

function eraseOne(id){
    const url = `http://localhost:3000/courses?id=${id}`;
    fetch(url,{
        method: 'delete',
        body: JSON.stringify({
        }),
        headers: {
            "Content-type": "application/json; charset=UTF-8"
        }
    })
        .then((response) => response.json())
        .then((data) => {
            console.log(data);
            getCourses();
            location.reload();
            
        })
};

function edit(id){
    const code = document.getElementById('code').value;
    const name = document.getElementById('name').value;
    const career = document.getElementById('career').value;
    const credits = document.getElementById('credits').value;
    fetch(`http://localhost:3000/courses?id=${id}`,{
        method: 'patch',
        body: JSON.stringify({
            code: code,
            name: name,
            career: career,
            credits: credits
        }),
        headers: {
            "Content-type": "application/json; charset=UTF-8"
        }
    })
    .then((response) => response.json())
    .then((data) => {
        console.log(data);
        window.history.back();
    }) 
}

function create(){
    const code = document.getElementById('code').value;
    const name = document.getElementById('name').value;
    const career = document.getElementById('career').value;
    const credits = document.getElementById('credits').value;
    fetch(`http://localhost:3000/courses`,{
        method: 'post',
        body: JSON.stringify({
            code: code,
            name: name,
            career: career,
            credits: credits
        }),
        headers: {
            "Content-type": "application/json; charset=UTF-8"
        }
    })
    .then((response) => response.json())
    .then((data) => {
        console.log(data);
        window.history.back();
    })  
    
}