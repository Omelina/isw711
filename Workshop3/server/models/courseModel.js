const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const course = new Schema({
    code: {
        type: String
    },
    name: {
        type: String
    },
    career: {
        type: String
    },
    credits: {
        type: Number
    }
});

module.exports = mongoose.model('course', course);