const express = require('express');
const cors = require("cors");
const app = express();
const mongoose = require("mongoose");
const db = mongoose.connect("mongodb://127.0.0.1:27017/workshop3");
const bodyParser = require("body-parser");
const Task = require("./models/courseModel");
const path = require('path');
const router = express.Router();


// check for cors
app.use(cors({
    domains: '*',
    methods: "*"
}));

app.get('/',function(req,res) {
    res.sendFile('index.html', { root: 'D:\\Documents\\ISW711\\Workshop3\\client'});
});

//add the router
app.use('/', router);
app.listen(process.env.port || 3001);

const {
    coursePatch,
    coursePost,
    courseGet,
    courseDelete
} = require("./controllers/courseController.js");

// parser for the request body (required for the POST and PUT methods)
app.use(bodyParser.json());

// listen to the task request
app.get("/courses", courseGet);
app.post("/courses", coursePost);
app.patch("/courses", coursePatch);
app.post("/courses", coursePatch);
app.delete("/courses", courseDelete);


app.listen(3000, () => console.log(`Example app listening on port 3000!`))