const Course = require("../models/courseModel");

const coursePost = (req, res) => {

    const course = new Course();

    course.code = req.body.code;
    course.name = req.body.name;
    course.career = req.body.career;
    course.credits = req.body.credits;

    if  (course.name && course.career && course.code && course.credits) {
     course.save(function (err) {
            if (err) {
                res.status(422);
                console.log('error while saving the course', err)
                res.json({
                    error: 'There was an error saving the course'
                });
            }
            res.status(201); //CREATED
            res.header({
                'location': `http://localhost:3000/courses/?id=$ course.id}`
            });
            res.json (course);
        });
    } else {
        res.status(422);
        console.log('error while saving the course')
        res.json({
            error: 'No valid data provided for course'
        });
    }
};

const courseGet = (req, res) => {
    // if an specific course is required
    if (req.query && req.query.id) {
        console.log(req.query.id)
        Course.findById(req.query.id, function (err, course) {
            if (err) {
                res.status(404);
                console.log('error while queryting the course', err)
                res.json({
                    error: "Course doesnt exist"
                })
            }
            console.log(course);
            res.json (course);
            
        });
    } else {
        // get all Courses
        Course.find(function (err, courses) {
            if (err) {
                res.status(422);
                res.json({
                    "error": err
                });
            }
            res.json(courses);
        });

    }
};

const coursePatch = (req, res) => {
    // get course by id
    if (req.query && req.query.id) {

        Course.findById(req.query.id, function (err, course) {
            if (err) {
                console.log(req.query.id);
                res.status(404);
                console.log('error while queryting the course', err);
                res.json({
                    error: "Course doesnt exist"
                });
            }

            // update the course object (patch)
            course.name = req.body.name ? req.body.name : course.name;
            course.code = req.body.code ? req.body.code : course.code;
            course.career = req.body.career ? req.body.career : course.career;
            course.credits = req.body.credits ? req.body.credits : course.credits;
            // update the course object (put)
            // course.name = req.body.name 
            //  course.code = req.body.code 
            //  course.career = req.body.career 
            //  course.credits = req.body.credits 

         course.save(function (err) {
                if (err) {
                    res.status(422);
                    console.log('error while saving the course', err)
                    res.json({
                        error: 'There was an error saving the course'
                    });
                }
                res.status(200); // OK
                res.json (course);
            });
        });
    } else {

        res.status(404);
        console.log(req.query.id);
        res.json({
            error: "Course doesnt exist"
        })
    }
};

const courseDelete = (req, res) => {
    // if an specific course is required
    if (req.query && req.query.id) {
        Course.findById(req.query.id, function (err, course) {
            if (err) {
                res.status(500);
                console.log('error while queryting the course', err)
                res.json({
                    error: "Course doesnt exist"
                })
            }
            //if the course exists
            if  (course) {
             course.remove(function (err) {
                    if (err) {
                        res.status(500).json({
                            message: "There was an error deleting the course"
                        });
                    }
                    res.status(204).json({});
                })
            } else {
                res.status(404);
                console.log('error while queryting the course', err)
                res.json({
                    error: "Course doesnt exist"
                })
            }
        });
    } else {
        res.status(404).json({
            error: "You must provide a course ID"
        });
    }
};

module.exports = {
    courseGet,
    coursePost,
    coursePatch,
    courseDelete
}